#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import logging
import argparse
import os

from mutagen.easyid3 import EasyID3
from mutagen.flac import FLAC
import mutagen

start = time.time()
compteur = 0

############ Arguments ############
parser = argparse.ArgumentParser()
optional = parser._action_groups.pop()
required = parser.add_argument_group('required arguments')
optional.add_argument("-a ", "--artist", help="Execute the script on this artist only.")
optional.add_argument("-c ", "--check", help="Run the script without applying the changes to the files.", action="store_true")
optional.add_argument("-d ", "--debug", help="Set logging level to DEBUG.", action="store_true")
parser._action_groups.append(optional)
args = parser.parse_args()

############ Logging ############
if args.debug:
    logging.basicConfig(level=logging.DEBUG)
    logging.debug('You set logging to DEBUG.')
else:
    logging.basicConfig(level=logging.INFO)

if args.check:
    logging.info('You set the check flag, no modification will be made to the files.')

############ Editing metadatas ############
cwd = os.getcwd()
logging.debug("Working directory : [%s]" % cwd)

# Getting list of artists
artistes = []
if args.artist is None:
    for item in os.listdir(cwd):
        if os.path.isdir(item):
            logging.debug("Found artist : [%s]" % item)
            artistePath = os.path.join(cwd, item)
            artistes.append(artistePath)
        else:
            logging.debug("[%s] is not a directory, skipping" % item)
    logging.info("Found [%s] artist(s)." % len(artistes))
else:
    if os.path.isdir(os.path.join(cwd, args.artist)):
        artistes.append(os.path.join(cwd, args.artist))
    else:
        logging.error("[%s] is not a directory, skipping" % args.artist)

for artiste in artistes:
    logging.debug("Working on : [%s]" % os.path.basename(artiste))
    # Getting list of albums
    albums = []
    for item in os.listdir(artiste):
        albumPath = os.path.join(artiste, item)
        if os.path.isdir(albumPath):
            logging.debug("Found album : [%s]" % item)
            albums.append(albumPath)
        else:
            logging.debug("[%s] is not a directory, skipping" % albumPath)
    logging.debug("Found [%s] album(s)." % len(albums))
    for album in albums:
        logging.debug("Working on : [%s] / [%s]" % (os.path.basename(artiste), os.path.basename(album)))
        # Getting list of songs
        songs = []
        for item in os.listdir(album):
            songPath = os.path.join(album, item)
            if os.path.isfile(songPath):
                logging.debug("Found song : [%s]" % item)
                songs.append(songPath)
            else:
                logging.debug("[%s] is not a file, skipping" % item)
        logging.debug("Found [%s] song(s)." % len(albums))
        # Editing metadatas
        for song in songs:
            logging.info("Working on : [%s] / [%s] / [%s]" % (os.path.basename(artiste), os.path.basename(album), os.path.basename(song)))

            try:
                #Dealing with mp3
                if song.endswith('.mp3'):
                    meta = EasyID3(song)
                #Dealing with flac
                elif song.endswith('.flac'):
                    meta = FLAC(song)
                #Unknown file type
                else:
                    logging.error("Unknown file type [%s]" % song)
                    continue
            except :
                meta = mutagen.File(song, easy=True)
                try:
                    meta.add_tags()
                except:
                    logging.error("Could not add tags on file [%s]" % song)
                    continue

            meta['artist'] = os.path.basename(artiste)
            meta['album'] = os.path.basename(album)
            if not args.check:
                #Dealing with mp3
                if song.endswith('.mp3'):
                    meta.save(song, v1=2)
                    changed = EasyID3(song)
                #Dealing with flac
                elif song.endswith('.flac'):
                    meta.save(song)
                    changed = FLAC(song)
            else:
                changed = meta
            compteur += 1
            logging.info("Artist : [%s]." % str(changed['artist'][0]))
            logging.info("Album : [%s]." % str(changed['album'][0]))

############ End ############
duree = time.time() - start
logging.info("Edited [%s] song(s) in [%s] second(s)." % (compteur, int(duree)))
