# Renommage musiques

## Description

Script de renommage massif des titres / artistes / albums de fichier audio.

Le script doit être lancé à la racine d'une arborescence de la forme suivante :
```bash
DossierMusiques
├── Slash
│   ├── Apocalyptic Love
│   │   ├── Anastasia.mp3
│   │   └── Apocalyptic_Love.mp3
│   └── World on Fire
│   │   ├── Shadow_Life.mp3
│   │   └── Automatic_Overdrive.mp3
├── Ultra Vomit
│   ├── Panzer Surprise !
│   ├── Objectif: Thunes
│   └── M. Patate
└── Renommage.py
```

Le script parcourt toute l'architecture et applique les modifications suivantes sur tous les fichiers audio :
  - Album = nom du dossier parent
  - Artiste = nom du dossier "grand-parent"

## Usage

```bash
usage: Renommage.py [-h] [-c] [-d]

optional arguments:
  -h, --help    show this help message and exit
  -c , --check  Run the script without applying the changes to the files.
  -d , --debug  Set logging level to DEBUG.
```

## Dépendances

- mutagen : https://github.com/quodlibet/mutage

## Divers

Notifications pour Telegram configurées
